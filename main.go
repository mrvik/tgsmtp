package main

import (
	"context"
	"flag"
	"fmt"
	"os"
	"os/signal"
	"tgsmtp/smtpProxy"

	"github.com/emersion/go-smtp"
	"github.com/sirupsen/logrus"
	"golang.org/x/sys/unix"
)

func main() {
	ctx, cancel := signal.NotifyContext(context.Background(), os.Interrupt, unix.SIGINT, unix.SIGTERM)
	defer cancel()

	logLevel := flag.String("log-level", logrus.WarnLevel.String(), "Log level for the application")

	flag.Parse()

	properLevel, err := logrus.ParseLevel(*logLevel)
	if err != nil {
		logrus.Errorf("Bad log level %s, using the default", *logLevel)
	} else {
		logrus.SetLevel(properLevel)
	}

	if err := run(ctx); err != nil {
		logrus.Fatal(err.Error())
	}
}

func run(ctx context.Context) error {
	server := smtp.NewServer(smtpProxy.Backend{})
	server.Addr = ":" + os.Getenv("PORT")
	server.EnableSMTPUTF8 = true
	server.AllowInsecureAuth = true

	if logrus.IsLevelEnabled(logrus.DebugLevel) {
		server.Debug = os.Stderr
	}

	go func() {
		if err := server.ListenAndServe(); err != nil {
			logrus.Fatalf("SMTP Server exited: %s", err)
		}
	}()

	logrus.WithField("addr", server.Addr).Info("Server listening")

	<-ctx.Done()

	if err := server.Close(); err != nil {
		return fmt.Errorf("close server: %w", err)
	}

	logrus.Info("Server gracefully closed")

	return nil
}
