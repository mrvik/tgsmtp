module tgsmtp

go 1.17

require (
	github.com/emersion/go-message v0.15.0
	github.com/emersion/go-smtp v0.15.0
	github.com/sirupsen/logrus v1.8.1
	golang.org/x/sys v0.0.0-20220224120231-95c6836cb0e7
	gopkg.in/tucnak/telebot.v2 v2.5.0
)

require (
	github.com/emersion/go-sasl v0.0.0-20200509203442-7bfe0ed36a21 // indirect
	github.com/emersion/go-textwrapper v0.0.0-20200911093747-65d896831594 // indirect
	github.com/pkg/errors v0.8.1 // indirect
)
