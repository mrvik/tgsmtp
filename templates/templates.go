package templates

import (
	"embed"
	"fmt"
	"strings"
	"text/template"
)

//go:embed mailTemplates
var mailTemplates embed.FS

// Template names.
const (
	MainMessage = "mainMessage.tmpl"
	MessagePart = "messagePart.tmpl"
)

type Tree struct {
	templateTree *template.Template
}

func NewTree() Tree {
	return Tree{
		templateTree: template.Must(template.ParseFS(mailTemplates, "mailTemplates/*.tmpl")),
	}
}

func (t Tree) RunTemplate(templateName string, params interface{}) (string, error) {
	var buf strings.Builder

	if err := t.templateTree.ExecuteTemplate(&buf, templateName, params); err != nil {
		return "", fmt.Errorf("execute mail template %q: %w", templateName, err)
	}

	return buf.String(), nil
}
