package smtpProxy

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"strings"
	"tgsmtp/templates"

	"github.com/emersion/go-message/mail"
	"github.com/sirupsen/logrus"
	"gopkg.in/tucnak/telebot.v2"
)

var (
	ErrUnknownHeader = errors.New("unknown header type")
)

var tmpl = templates.NewTree()

type MessagePart struct {
	Sendable   interface{}
	Formatting telebot.ParseMode
}

func readParts(r io.Reader, defaultFrom string) ([]MessagePart, error) {
	reader, err := mail.CreateReader(r)
	if err != nil {
		return nil, fmt.Errorf("start reading mail: %w", err)
	}

	var parts []MessagePart

	for {
		part, err := reader.NextPart()
		if errors.Is(err, io.EOF) {
			break
		} else if err != nil {
			return parts, fmt.Errorf("read part %d: %w", len(parts), err)
		}

		sendable, err := getSendable(part, defaultFrom, len(parts))
		if err != nil {
			return parts, err
		}

		if sendable != nil {
			logrus.Debugf("Adding sendable %#v", *sendable)

			parts = append(parts, *sendable)
		}
	}

	return parts, nil
}

type MailHeader struct {
	From    string
	To      string
	CC      string
	Subject string

	Content string
}

type AttachmentOptions struct {
	PartNo             int
	ContentDescription string
	Data               string
}

func formatAddressList(l []*mail.Address) string {
	names := make([]string, 0, len(l))

	for _, a := range l {
		if a == nil {
			continue
		}

		names = append(names, formatAddress(a))
	}

	return strings.Join(names, ", ")
}

func formatAddress(a *mail.Address) string {
	return fmt.Sprintf("%s <%s>", a.Name, a.Address)
}

func getSendable(part *mail.Part, defaultFrom string, partNumber int) (*MessagePart, error) {
	body, err := io.ReadAll(part.Body)
	if err != nil {
		return nil, fmt.Errorf("read part %d body: %w", partNumber, err)
	}

	switch h := part.Header.(type) {
	case *mail.InlineHeader:
		return getSendableHeader(h, body, defaultFrom)
	case *mail.AttachmentHeader:
		return getSendablePart(h, body, partNumber)
	}

	return nil, fmt.Errorf("%w: got header of type %T", ErrUnknownHeader, part.Header)
}

func defaultString(strings ...string) string {
	for _, s := range strings {
		if s != "" {
			return s
		}
	}

	return ""
}

func getSendableHeader(header *mail.InlineHeader, body []byte, defaultFrom string) (*MessagePart, error) {
	from, err := mail.ParseAddress(defaultString(header.Get("From"), defaultFrom))
	if err != nil {
		return nil, fmt.Errorf("parse from asddress: %w", err)
	}

	to, _ := mail.ParseAddressList(header.Get("To"))
	cc, _ := mail.ParseAddressList(header.Get("Cc"))
	subject := header.Get("Subject")

	options := MailHeader{
		From:    formatAddress(from),
		To:      formatAddressList(to),
		CC:      formatAddressList(cc),
		Subject: subject,
		Content: string(body),
	}

	content, err := tmpl.RunTemplate(templates.MainMessage, options)
	if err != nil {
		return nil, fmt.Errorf("execute main part template: %w", err)
	}

	return &MessagePart{
		Sendable:   content,
		Formatting: telebot.ModeMarkdown,
	}, nil
}

func getSendablePart(header *mail.AttachmentHeader, body []byte, partNumber int) (*MessagePart, error) {
	contentType := header.Get("Content-Type")
	contentDescription := header.Get("Content-Description")
	mimeType := strings.SplitN(contentType, ";", 2)[0] //nolint: gomnd

	attachmentOptions := AttachmentOptions{
		PartNo:             partNumber,
		ContentDescription: contentDescription,
	}
	telebotFormat := telebot.ModeMarkdown

	logrus.Debugf("Received Content-Type = %s for %s", mimeType, body)

	switch mimeType {
	case "multipart/alternative":
		return nil, nil // Not interested in meta content
	case "text/html":
		telebotFormat = telebot.ModeHTML

		fallthrough
	case "text/plain":
		attachmentOptions.Data = string(body)
	default:
		filename, _ := header.Filename()

		return &MessagePart{
			Sendable: &telebot.Document{
				File: telebot.File{
					FileReader: bytes.NewReader(body),
				},
				FileName: filename,
			},
		}, nil
	}

	content, err := tmpl.RunTemplate(templates.MessagePart, attachmentOptions)
	if err != nil {
		return nil, fmt.Errorf("execute part template: %w", err)
	}

	return &MessagePart{
		Sendable:   content,
		Formatting: telebotFormat,
	}, nil
}
