package smtpProxy

import (
	"errors"
	"fmt"
	"io"
	"strconv"
	"strings"

	"github.com/emersion/go-smtp"
	"github.com/sirupsen/logrus"
	"gopkg.in/tucnak/telebot.v2"
)

var (
	ErrNotAuthenticated = fmt.Errorf("%w: didn't authenticate telegram bot", smtp.ErrAuthRequired)
	ErrNoRecipients     = errors.New("no recipients specified")
	ErrBadContent       = errors.New("unsupported content received")
	ErrNoMessages       = errors.New("no message parts found in the current mail")
)

type Backend struct{}

// Authenticate a user. Return smtp.ErrAuthUnsupported if you don't want to
// support this.
func (b Backend) Login(state *smtp.ConnectionState, username string, password string) (smtp.Session, error) {
	if username == "" || password == "" {
		return nil, fmt.Errorf("%w: must set username and password", smtp.ErrAuthRequired)
	}

	return NewSession(password)
}

// Called if the client attempts to send mail without logging in first.
// Return smtp.ErrAuthRequired if you don't want to support this.
func (b Backend) AnonymousLogin(state *smtp.ConnectionState) (smtp.Session, error) {
	return nil, smtp.ErrAuthRequired
}

type Session struct {
	defaultFrom string
	bot         *telebot.Bot
	parts       []MessagePart
	recipients  []telebot.Recipient
}

func NewSession(botToken string) (*Session, error) {
	logrus.Info("Starting new session")

	bot, err := telebot.NewBot(telebot.Settings{
		Token:   botToken,
		Updates: 1,
	})
	if err != nil {
		return nil, fmt.Errorf("create telegram bot for session: %w", err)
	}

	return &Session{bot: bot}, nil
}

// Discard currently processed message.
func (s *Session) Reset() {
	s.recipients = nil
	s.parts = nil
}

// Free all resources associated with session.
func (s *Session) Logout() error {
	if s.bot != nil {
		s.bot.Close()

		s.bot = nil
	}

	s.Reset()

	return nil
}

// Set return path for currently processed message.
func (s *Session) Mail(from string, opts smtp.MailOptions) error {
	s.defaultFrom = from

	return nil
}

func (s *Session) sendMail() error {
	if s.bot == nil {
		return fmt.Errorf("send mail: %w", ErrNotAuthenticated)
	}

	if len(s.recipients) == 0 {
		return fmt.Errorf("send mail: %w", ErrNoRecipients)
	}

	if len(s.parts) == 0 {
		return fmt.Errorf("send mail: %w", ErrNoMessages)
	}

	logrus.Debugf("Send messages:\n%#v", s.parts)

	var errors []error

	for _, r := range s.recipients {
		if err := s.sendMessages(r); err != nil {
			errors = append(errors, err)
		}
	}

	if len(errors) > 0 {
		return MultiErrors(errors)
	}

	return nil
}

func (s *Session) sendMessages(to telebot.Recipient) error {
	// Linking each message as a response to the previous one.
	var lastMessage *telebot.Message

	for _, part := range s.parts {
		options := &telebot.SendOptions{
			ReplyTo:   lastMessage,
			ParseMode: part.Formatting,
		}

		msg, err := s.bot.Send(to, part.Sendable, options)
		if err != nil {
			return fmt.Errorf("send message: %w", err)
		}

		lastMessage = msg
	}

	return nil
}

// Add recipient for currently processed message.
func (s *Session) Rcpt(to string) error {
	if s.bot == nil {
		return fmt.Errorf("find recipient: %w", ErrNotAuthenticated)
	}

	username := strings.SplitN(to, "@", 2)[0] //nolint: gomnd

	if _, err := strconv.Atoi(username); err != nil {
		username = "@" + username // Not an ID
	}

	logrus.WithField("target", username).Info("Adding new target")

	chat, err := s.bot.ChatByID(username)
	if err != nil {
		return fmt.Errorf("find chat: %w", err)
	}

	s.recipients = append(s.recipients, chat)

	return nil
}

// Set currently processed message contents and send it.
func (s *Session) Data(r io.Reader) error {
	parts, err := readParts(r, s.defaultFrom)
	if err != nil {
		return fmt.Errorf("read message parts: %w", err)
	}

	s.parts = parts

	return s.sendMail()
}

type MultiErrors []error

func (s MultiErrors) Error() string {
	return fmt.Sprintf("multiple errors (%d):\n%s", len(s), s.formatErrors())
}

func (s MultiErrors) formatErrors() string {
	var builder strings.Builder

	for _, e := range s {
		builder.WriteString("\t" + e.Error() + "\n")
	}

	return builder.String()
}
